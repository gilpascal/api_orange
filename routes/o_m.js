require('dotenv').config();
var request = require('request')
var express = require('express')
const uuid  = require('uuid');

var router = express.Router()
var HttpStatus = require('../HttpStatus/http_status')

router.get("/token", (req,res)=>{
      const options = {
        url: process.env.URL_TOKEN,
        method:'POST',
        headers:{
          "Content-Type":"application/x-www-form-urlencoded",
           "Authorization" : "Basic " + process.env.Authorization         
        },
        form: {
        grant_type: process.env.grant_type,
        }
    }
    request(options,(err,response,body)=>{
      
       res.send(body)
        
    })


});





router.post("/url",(req,res)=>{
  const bod = {
      'merchant_key': process.env.Merchant_Key,
      'currency'   : 'XOF',
      'order_id'   : req.body.order_id,
      'amount'     : req.body.amount, 
      'return_url' : 'https://aladin.ci/home', 
      'cancel_url' : 'https://aladin.ci/home',  
      'notif_url'  : 'https://aladin.ci/home',    
      'lang'       : 'fr',
      'reference'  : 'ALADIN TECHNOLOGY SARL'

    }
    console.log(req.headers.authorization)
    const options = {
      
        url: process.env.URL_PAY ,
        headers: {
          'Content-Type':'application/json',
          'Authorization':req.headers.authorization,
          'Accept':'application/json',
          'Cache-Control': 'no-cache'

        },


        body:JSON.stringify(bod),
        method:"POST"
    }    
    request(options,(err,resp)=>{
      if(resp.body){
        res.send(resp.body)
      
      }else{
        if(resp.body.code){

          res.send(resp.body.message)

        }else{

          res.send(resp.body.message)
          
        }
        
      }

    })

})

router.post("/transaction/status",(req,res)=>{
  
  const bo = {
    'order_id': req.body.order_id,
    'amount'   :  req.body.amount,
    'pay_token' :  req.body.pay_token
   
  }
  const options = {
    url: process.env.URL_STATUS ,
    headers: {
      'Content-Type':'application/json',
      'Authorization':req.headers.authorization,
      'Accept':'application/json'      

    },

    body:JSON.stringify(bo),
    method:"POST"
  }
  request(options,(error,resp)=>{
    console.log(resp)
    if(resp.statusCode==201){
      res.send({
        status:true,
        code:resp.statusCode,
        data:JSON.parse(resp.body)
      })
    }else{
      res.send({
        status:false,
        code:resp.statusCode,
        err:error
        
      })
    }
  })

})


module.exports = router;